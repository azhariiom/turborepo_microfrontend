import NextFederationPlugin from "@module-federation/nextjs-mf";

/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  swcMinify: true,
  experimental: {
    scrollRestoration: true,
  },
  images: {
    remotePatterns: [
      {
        protocol: "https",
        hostname: "**.unsplash.com",
      },
    ],
  },
  transpilePackages: ["@repo/ui"],
  webpack(config, options) {
    config.cache = false;
      config.plugins.push(
        new NextFederationPlugin({
          name: "shop",
          remotes: {},
          filename: "static/chunks/remoteEntry.js",
          exposes: {
            './catalog': "./components/Catalog.tsx"
          }
        })
      );
  
      return config;
  }
};

export default nextConfig;
