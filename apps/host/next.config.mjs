import NextFederationPlugin from "@module-federation/nextjs-mf";

//https://main.d2adc6yjcg646s.amplifyapp.com

const SHOP_APP_URL = "https://localhost:3051";

const remotes = (isServer) => {
  const location = isServer ? "ssr" : "chunks";
  return {
    shop: `shop@${SHOP_APP_URL}/_next/static/${location}/remoteEntry.js`
  };
};

/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  swcMinify: true,
  experimental: {
    scrollRestoration: true,
  },
  images: {
    remotePatterns: [
      {
        protocol: "https",
        hostname: "**.unsplash.com",
      },
    ],
  },
  transpilePackages: ["@repo/ui"],
  webpack(config, { isServer }) {
    config.cache = false;
      config.plugins.push(
        new NextFederationPlugin({
          name: "host",
          remotes: remotes(isServer),
          filename: "static/chunks/remoteEntry.js",
          exposes: {}
        })
      );
  
      return config;
  }
};

export default nextConfig;
